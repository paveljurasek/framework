<?php

namespace JedenWeb\Tests;

use Nette\ObjectMixin;

/**
 * @author  Jiří Šifalda <sifalda.jiri@gmail.com>
 */
abstract class TestCase extends \PHPUnit_Framework_TestCase
{

	/**
	 * @var \SystemContainer|\Nette\DI\Container
	 */
	private $context;



	public function __construct($name = NULL, array $data = array(), $dataName = '')
	{
		$this->context = Configurator::getTestsContainer();

		parent::__construct($name, $data, $dataName);
	}

	/**
	 * @return \Nette\DI\Container|\SystemContainer
	 */
	protected function getContext()
	{
		return $this->context;
	}

	/**
	 * @param $name
	 * @param null $default
	 * @return null
	 */
	protected function getContextParameter($name, $default = null)
	{
		$params = $this->getContext()->getParameters();
		return (isset($params[$name])) ? $params[$name] : $default;
	}

	/**
	 * @param string $class
	 * @param string $name
	 * @return \ReflectionMethod
	 */
	protected function getProtectedClassMethod($class, $name) {
		$class = new \ReflectionClass($class);
		$method = $class->getMethod($name);
		$method->setAccessible(true);
		return $method;
	}

	/********************* Nette\Object behaviour ****************d*g**/
	/**
	 * @return \Nette\Reflection\ClassType
	 */
	public static function getReflection()
	{
		return new \Nette\Reflection\ClassType(get_called_class());
	}

	/**
	 * @param string $name
	 *
	 * @return mixed
	 */
	public function &__get($name)
	{
		return ObjectMixin::get($this, $name);
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 */
	public function __set($name, $value)
	{
		ObjectMixin::set($this, $name, $value);
	}

	/**
	 * @param string $name
	 * @param array $args
	 * @return mixed
	 */
	public function __call($name, $args)
	{
		return ObjectMixin::call($this, $name, $args);
	}

	/**
	 * @param string $name
	 *
	 * @return bool
	 */
	public function __isset($name)
	{
		return ObjectMixin::has($this, $name);
	}

	/**
	 * @param string $name
	 */
	public function __unset($name)
	{
		ObjectMixin::remove($this, $name);
	}

}
